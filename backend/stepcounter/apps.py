from django.apps import AppConfig


class StepcounterConfig(AppConfig):
    name = 'stepcounter'
