from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class StepModel(models.Model):
    """A class defining the step records of a User"""
    date = models.DateField(primary_key = True, unique = True)

    step_count = models.IntegerField(help_text = 'The number of steps in a day')
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    